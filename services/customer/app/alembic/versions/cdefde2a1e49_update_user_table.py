"""Update user table

Revision ID: cdefde2a1e49
Revises: 8ca222738985
Create Date: 2022-01-31 15:30:43.751578

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cdefde2a1e49'
down_revision = '8ca222738985'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('created_at', sa.DateTime(), nullable=True))
    op.add_column('user', sa.Column('updated_at', sa.DateTime(), nullable=True))
    op.add_column('user', sa.Column('phone', sa.String(length=30), nullable=False))
    op.alter_column('user', 'full_name',
               existing_type=sa.VARCHAR(),
               nullable=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('user', 'full_name',
               existing_type=sa.VARCHAR(),
               nullable=True)
    op.drop_column('user', 'phone')
    op.drop_column('user', 'updated_at')
    op.drop_column('user', 'created_at')
    # ### end Alembic commands ###
