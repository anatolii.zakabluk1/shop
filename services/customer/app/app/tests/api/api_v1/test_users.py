from typing import Dict

from fastapi import status
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from app import crud
from app.core.config import settings
from app.tests.utils.user import create_user, user_authentication_headers
from app.tests.utils.utils import random_email, random_lower_string


async def test_get_users_superuser_me(
    client: AsyncClient, superuser_token_headers: Dict[str, str]
) -> None:
    r = await client.get(
        f"{settings.API_V1_STR}/users/me", headers=superuser_token_headers
    )
    current_user = r.json()
    assert current_user
    assert current_user["is_active"] is True
    assert current_user["is_superuser"]
    assert current_user["email"] == settings.FIRST_SUPERUSER


async def test_get_users_normal_user_me(
    client: AsyncClient, normal_user_token_headers: Dict[str, str]
) -> None:
    r = await client.get(
        f"{settings.API_V1_STR}/users/me", headers=normal_user_token_headers
    )
    current_user = r.json()
    assert current_user
    assert current_user["is_active"] is True
    assert current_user["is_superuser"] is False
    assert current_user["email"] == settings.EMAIL_TEST_USER


async def test_create_user_new_email(
    client: AsyncClient, superuser_token_headers: dict, db: AsyncSession
) -> None:
    email = random_email()
    password = random_lower_string()
    data = {"email": email, "new_password1": password, "new_password2": password}
    r = await client.post(
        f"{settings.API_V1_STR}/users/", headers=superuser_token_headers, json=data,
    )
    assert status.HTTP_200_OK <= r.status_code < status.HTTP_300_MULTIPLE_CHOICES
    created_user = r.json()
    user = await crud.user.get_by_email(db, email=email)
    assert user
    assert user.email == created_user["email"]


async def test_create_user_existing_email(
    client: AsyncClient, superuser_token_headers: dict, db: AsyncSession
) -> None:
    email = random_email()
    password = random_lower_string()
    await create_user(db=db, email=email, password=password)
    data = {"email": email, "new_password1": password, "new_password2": password}
    r = await client.post(
        f"{settings.API_V1_STR}/users/", headers=superuser_token_headers, json=data,
    )
    created_user = r.json()
    assert r.status_code == status.HTTP_400_BAD_REQUEST
    assert "_id" not in created_user


async def test_create_user_by_normal_user(
    client: AsyncClient, normal_user_token_headers: Dict[str, str]
) -> None:
    email = random_email()
    password = random_lower_string()
    data = {"email": email, "new_password1": password, "new_password2": password}
    r = await client.post(
        f"{settings.API_V1_STR}/users/", headers=normal_user_token_headers, json=data,
    )
    assert r.status_code == status.HTTP_403_FORBIDDEN
    assert r.json() == {"detail": "The user doesn't have enough privileges"}


async def test_create_user_open(client: AsyncClient, db: AsyncSession) -> None:
    email = random_email()
    password = random_lower_string()
    data = {"email": email, "password": password}
    settings.USERS_OPEN_REGISTRATION = False
    r = await client.post(f"{settings.API_V1_STR}/users/open", json=data)
    assert r.status_code == status.HTTP_403_FORBIDDEN
    assert r.json() == {"detail": "Open user registration is forbidden on this server"}

    settings.USERS_OPEN_REGISTRATION = True
    email = random_email()
    password = random_lower_string()
    await create_user(db=db, email=email, password=password)
    data = {"email": email, "password": password}
    r = await client.post(f"{settings.API_V1_STR}/users/open", json=data)
    assert r.status_code == status.HTTP_400_BAD_REQUEST
    detail = "The user with this email already exists in the system"
    assert r.json() == {"detail": detail}

    data = {"email": random_email(), "password": random_lower_string()}
    r = await client.post(f"{settings.API_V1_STR}/users/open", json=data)
    assert r.status_code == status.HTTP_200_OK
    assert r.json()['email'] == data["email"]


async def test_get_existing_user(
    client: AsyncClient, superuser_token_headers: dict, db: AsyncSession
) -> None:
    email = random_email()
    password = random_lower_string()
    user = await create_user(db=db, email=email, password=password)
    user_id = user.id
    r = await client.get(
        f"{settings.API_V1_STR}/users/{user_id}", headers=superuser_token_headers,
    )
    assert status.HTTP_200_OK <= r.status_code < status.HTTP_300_MULTIPLE_CHOICES
    api_user = r.json()
    existing_user = await crud.user.get_by_email(db, email=email)
    assert existing_user
    assert existing_user.email == api_user["email"]


async def test_retrieve_users(
    client: AsyncClient, superuser_token_headers: dict, db: AsyncSession
) -> None:
    email = random_email()
    password = random_lower_string()
    await create_user(db=db, email=email, password=password)

    email = random_email()
    password = random_lower_string()
    await create_user(db=db, email=email, password=password)

    r = await client.get(
        f"{settings.API_V1_STR}/users/", headers=superuser_token_headers
    )
    all_users = r.json()

    assert len(all_users) > 1


async def test_update_user_me(client: AsyncClient, db: AsyncSession) -> None:
    email = random_email()
    password = random_lower_string()
    user = await create_user(db=db, email=email, password=password)
    data = {"full_name": "Test User", "phone": "1-770-736-8031"}
    headers = await user_authentication_headers(
        client=client, email=email, password=password
    )
    r = await client.put(f"{settings.API_V1_STR}/users/me", json=data, headers=headers)
    updated_user = r.json()
    assert r.status_code == status.HTTP_200_OK
    await db.refresh(user)
    assert user.full_name == updated_user["full_name"]
    assert user.phone == updated_user["phone"]


async def test_update_user(
    client: AsyncClient, superuser_token_headers: dict, db: AsyncSession
) -> None:
    email = random_email()
    password = random_lower_string()
    user = await create_user(db=db, email=email, password=password)
    data = {"full_name": "Test User", "phone": "1-770-736-8031"}
    headers = await user_authentication_headers(
        client=client, email=email, password=password
    )
    r = await client.put(
        f"{settings.API_V1_STR}/users/{user.id}", json=data, headers=headers
    )
    assert r.status_code == status.HTTP_403_FORBIDDEN
    assert r.json() == {"detail": "The user doesn't have enough privileges"}
    r = await client.put(
        f"{settings.API_V1_STR}/users/{user.id}",
        json=data,
        headers=superuser_token_headers
    )
    updated_user = r.json()
    await db.refresh(user)
    assert user.full_name == updated_user["full_name"]
    assert user.phone == updated_user["phone"]


async def test_reset_password_me(client: AsyncClient, db: AsyncSession) -> None:
    email = random_email()
    password = random_lower_string()
    await create_user(db=db, email=email, password=password)
    data = {
        "old_password": password,
        "new_password1": "password1",
        "new_password2": "password2"
    }
    headers = await user_authentication_headers(
        client=client, email=email, password=password
    )
    r = await client.patch(
        f"{settings.API_V1_STR}/users/update-password/me", json=data, headers=headers
    )
    assert r.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    response = r.json()
    assert "detail" in response
    assert "msg" in response["detail"][0]
    assert response["detail"][0]["msg"] == "Passwords do not match"
    data = {
        "old_password": 'wrong_old_password',
        "new_password1": "password",
        "new_password2": "password"
    }
    r = await client.patch(
        f"{settings.API_V1_STR}/users/update-password/me", json=data, headers=headers
    )
    assert r.status_code == status.HTTP_400_BAD_REQUEST
    assert r.json() == {"detail": "Incorrect current password"}
    data = {
        "old_password": password,
        "new_password1": "new_password",
        "new_password2": "new_password"
    }
    r = await client.patch(
        f"{settings.API_V1_STR}/users/update-password/me", json=data, headers=headers
    )
    assert r.status_code == status.HTTP_200_OK
    assert r.json() == {"msg": "Password updated successfully"}


async def test_reset_password(
    client: AsyncClient, superuser_token_headers: dict, db: AsyncSession
) -> None:
    email = random_email()
    password = random_lower_string()
    user = await create_user(db=db, email=email, password=password)
    data = {
        "old_password": password,
        "new_password1": "new_password",
        "new_password2": "new_password"
    }
    headers = await user_authentication_headers(
        client=client, email=email, password=password
    )
    r = await client.patch(
        f"{settings.API_V1_STR}/users/update-password/{user.id}",
        json=data,
        headers=headers
    )
    assert r.status_code == status.HTTP_403_FORBIDDEN
    assert r.json() == {"detail": "The user doesn't have enough privileges"}
    r = await client.patch(
        f"{settings.API_V1_STR}/users/update-password/{user.id}",
        json=data,
        headers=superuser_token_headers
    )
    assert r.status_code == status.HTTP_200_OK
    assert r.json() == {"msg": "Password updated successfully"}


async def test_delete_user_by_normal_user(
    client: AsyncClient, superuser_token_headers: dict, db: AsyncSession
) -> None:
    email = random_email()
    password = random_lower_string()
    user = await create_user(db=db, email=email, password=password)
    headers = await user_authentication_headers(
        client=client, email=email, password=password
    )
    r = await client.delete(f"{settings.API_V1_STR}/users/{user.id}", headers=headers)
    assert r.status_code == status.HTTP_403_FORBIDDEN
    assert r.json() == {"detail": "The user doesn't have enough privileges"}
    r = await client.delete(
        f"{settings.API_V1_STR}/users/{user.id}", headers=superuser_token_headers
    )
    assert r.status_code == status.HTTP_200_OK
    assert r.json()["id"] == user.id
