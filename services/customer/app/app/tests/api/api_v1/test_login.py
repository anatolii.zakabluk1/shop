from typing import Dict

from fastapi import status
from httpx import AsyncClient

from app.core.config import settings
from app.tests.utils.utils import random_email, random_lower_string


async def test_get_access_token(client: AsyncClient) -> None:
    login_data = {
        "username": settings.FIRST_SUPERUSER,
        "password": settings.FIRST_SUPERUSER_PASSWORD,
    }
    r = await client.post(f"{settings.API_V1_STR}/login/access-token", data=login_data)
    tokens = r.json()
    assert r.status_code == status.HTTP_200_OK
    assert "access_token" in tokens
    assert tokens["access_token"]
    assert "token_type" in tokens
    assert tokens["token_type"] == "bearer"


async def test_use_access_token(
    client: AsyncClient, superuser_token_headers: Dict[str, str]
) -> None:
    r = await client.post(
        f"{settings.API_V1_STR}/login/test-token", headers=superuser_token_headers,
    )
    result = r.json()
    assert r.status_code == status.HTTP_200_OK
    assert "email" in result


async def test_register(client: AsyncClient) -> None:
    email = random_email()
    password = random_lower_string()
    data = {"email": email, "new_password1": password, "new_password2": password}
    r = await client.post(f"{settings.API_V1_STR}/register", json=data)
    assert r.status_code == status.HTTP_200_OK
    tokens = r.json()
    assert "access_token" in tokens
    assert tokens["access_token"]
    assert "token_type" in tokens
    assert tokens["token_type"] == "bearer"
    r = await client.post(f"{settings.API_V1_STR}/register", json=data)
    assert r.status_code == status.HTTP_400_BAD_REQUEST
    details = "The user with this email already exists in the system."
    assert r.json() == {"detail": details}
