import os
from app.core.config import Settings
import pytest


@pytest.mark.parametrize(
    "urls",
    [
        '"http://127.0.0.1"',
        '["http://127.0.0.1"]',
        '"http://127.0.0.1, http://172.26.22.2"',
        '["http://127.0.0.1","http://172.26.22.2"]',
    ],
)
def test_set_cors_allows(urls: str) -> None:
    os.environ["BACKEND_CORS_ORIGINS"] = urls
    try:
        Settings()
    except ValueError:
        assert False
    else:
        assert True


def test_set_cors_allows_failure() -> None:
    os.environ["BACKEND_CORS_ORIGINS"] = "2130706433"  # 127.0.0.1
    try:
        Settings()
    except ValueError:
        assert True
    else:
        assert False
