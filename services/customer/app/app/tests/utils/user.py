from typing import Dict

from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from app import crud
from app.core.config import settings
from app.models.user import User
from app.schemas.user import UserCreate, UserUpdate
from app.tests.utils.utils import random_lower_string


async def create_user(*, db: AsyncSession, email: str, password: str) -> User:
    user_in = UserCreate(email=email, new_password1=password, new_password2=password)
    user = await crud.user.create(db=db, obj_in=user_in)
    return user


async def user_authentication_headers(
    *, client: AsyncClient, email: str, password: str
) -> Dict[str, str]:
    data = {"username": email, "password": password}

    r = await client.post(f"{settings.API_V1_STR}/login/access-token", data=data)
    response = r.json()
    auth_token = response["access_token"]
    headers = {"Authorization": f"Bearer {auth_token}"}
    return headers


async def get_user_token_headers_from_email(
    *, client: AsyncClient, email: str, db: AsyncSession
) -> Dict[str, str]:
    """
    Return a valid token for the user with given email.

    If the user doesn't exist it is created first.
    """
    password = random_lower_string()
    user = await crud.user.get_by_email(db, email=email)
    if not user:
        user_in_create = UserCreate(email=email, new_password1=password,
                                    new_password2=password)
        user = await crud.user.create(db, obj_in=user_in_create)
    else:
        user_in_update = UserUpdate(password=password)
        user = await crud.user.update(db, db_obj=user, obj_in=user_in_update)

    return await user_authentication_headers(
        client=client, email=email, password=password
    )


async def get_superuser_token_headers(
    *, client: AsyncClient, db: AsyncSession
) -> Dict[str, str]:
    """
    Return a valid token for the superuser with given email.

    If the superuser doesn't exist it is created first.
    """
    email = settings.FIRST_SUPERUSER
    password = settings.FIRST_SUPERUSER_PASSWORD
    user = await crud.user.get_by_email(db, email=settings.FIRST_SUPERUSER)
    if not user:
        user_in_create = UserCreate(email=email, new_password1=password,
                                    new_password2=password, is_superuser=True)
        user = await crud.user.create(db, obj_in=user_in_create)

    return await user_authentication_headers(
        client=client, email=email, password=password
    )
