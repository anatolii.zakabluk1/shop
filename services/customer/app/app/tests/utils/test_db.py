from pydantic import PostgresDsn
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text
from sqlalchemy.exc import OperationalError, ProgrammingError
from sqlalchemy.ext.asyncio import AsyncSession, AsyncEngine, create_async_engine

from app.core.config import settings
from app.tests.utils import event_loop


TEST_DB_NAME = f"{settings.POSTGRES_DB}_test"
SQLALCHEMY_DATABASE_URI = PostgresDsn.build(
    scheme="postgresql+asyncpg",
    user=settings.POSTGRES_USER,
    password=settings.POSTGRES_PASSWORD,
    host=settings.POSTGRES_SERVER,
    path=f"/{TEST_DB_NAME}"
)

SQLALCHEMY_DATABASE_URL = PostgresDsn.build(
    scheme="postgresql+asyncpg",
    user=settings.POSTGRES_USER,
    password=settings.POSTGRES_PASSWORD,
    host=settings.POSTGRES_SERVER,
)


async def test_database_exists() -> bool:
    query = text(f"SELECT 1 FROM pg_database WHERE datname='{TEST_DB_NAME}'")
    en = create_async_engine(SQLALCHEMY_DATABASE_URL)
    try:
        async with en.connect() as conn:
            result = await conn.scalar(query)
            return bool(result)
    except (ProgrammingError, OperationalError):
        return False


async def create_test_database() -> None:
    en = create_async_engine(SQLALCHEMY_DATABASE_URL)
    async with en.connect() as conn:
        await conn.execute(text("COMMIT"))
        await conn.execute(text(f"CREATE DATABASE {TEST_DB_NAME}"))


async def get_async_engine() -> AsyncEngine:
    if not await test_database_exists():
        await create_test_database()
    return create_async_engine(SQLALCHEMY_DATABASE_URI)


loop = event_loop.get_loop()
engine = loop.run_until_complete(get_async_engine())

TestingAsyncSessionLocal = sessionmaker(
    engine, class_=AsyncSession, expire_on_commit=False
)
