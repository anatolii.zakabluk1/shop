from sqlalchemy.ext.asyncio import AsyncSession

from app import crud, schemas
from app.core.config import settings


async def init_db(db: AsyncSession) -> None:
    user = await crud.user.get_by_email(db, email=settings.FIRST_SUPERUSER)
    if not user:
        user_in = schemas.UserCreate(
            email=settings.FIRST_SUPERUSER,
            new_password1=settings.FIRST_SUPERUSER_PASSWORD,
            new_password2=settings.FIRST_SUPERUSER_PASSWORD,
            is_superuser=True,
        )
        user = await crud.user.create(db, obj_in=user_in)  # noqa: F841
