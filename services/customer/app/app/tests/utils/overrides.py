from typing import Generator

from app.tests.utils.test_db import TestingAsyncSessionLocal


async def override_get_db() -> Generator:
    async with TestingAsyncSessionLocal() as session:
        yield session
