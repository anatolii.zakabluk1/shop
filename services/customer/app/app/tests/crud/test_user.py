from fastapi.encoders import jsonable_encoder
from sqlalchemy.ext.asyncio import AsyncSession

from app import crud
from app.core.security import verify_password
from app.schemas.user import UserCreate, UserUpdate
from app.tests.utils.utils import random_email, random_lower_string


async def test_create_user(db: AsyncSession) -> None:
    email = random_email()
    password = random_lower_string()
    user_in = UserCreate(email=email, new_password1=password, new_password2=password)
    user = await crud.user.create(db, obj_in=user_in)
    assert user.email == email
    assert hasattr(user, "hashed_password")


async def test_check_if_user_is_active(db: AsyncSession) -> None:
    email = random_email()
    password = random_lower_string()
    user_in = UserCreate(email=email, new_password1=password, new_password2=password)
    user = await crud.user.create(db, obj_in=user_in)
    is_active = crud.user.is_active(user)
    assert is_active is True


async def test_check_if_user_is_active_inactive(db: AsyncSession) -> None:
    email = random_email()
    password = random_lower_string()
    user_in = UserCreate(email=email, new_password1=password,
                         new_password2=password, disabled=True)
    user = await crud.user.create(db, obj_in=user_in)
    is_active = crud.user.is_active(user)
    assert is_active


async def test_check_if_user_is_superuser(db: AsyncSession) -> None:
    email = random_email()
    password = random_lower_string()
    user_in = UserCreate(email=email, new_password1=password,
                         new_password2=password, is_superuser=True)
    user = await crud.user.create(db, obj_in=user_in)
    is_superuser = crud.user.is_superuser(user)
    assert is_superuser is True


async def test_check_if_user_is_superuser_normal_user(db: AsyncSession) -> None:
    username = random_email()
    password = random_lower_string()
    user_in = UserCreate(email=username, new_password1=password, new_password2=password)
    user = await crud.user.create(db, obj_in=user_in)
    is_superuser = crud.user.is_superuser(user)
    assert is_superuser is False


async def test_get_user(db: AsyncSession) -> None:
    password = random_lower_string()
    username = random_email()
    user_in = UserCreate(email=username, new_password1=password,
                         new_password2=password, is_superuser=True)
    user = await crud.user.create(db, obj_in=user_in)
    user_2 = await crud.user.get(db, id=user.id)
    assert user_2
    assert user.email == user_2.email
    assert jsonable_encoder(user) == jsonable_encoder(user_2)


async def test_update_user(db: AsyncSession) -> None:
    password = random_lower_string()
    email = random_email()
    user_in = UserCreate(email=email, new_password1=password,
                         new_password2=password, full_name="Test User")
    user = await crud.user.create(db, obj_in=user_in)
    new_full_name = "Test User2"
    user_in_update = UserUpdate(full_name=new_full_name)
    await crud.user.update(db, db_obj=user, obj_in=user_in_update)
    await db.refresh(user)
    assert user.email == email
    assert user.full_name == new_full_name
    new_password = random_lower_string()
    data_in = {'password': new_password}
    await crud.user.update(db, db_obj=user, obj_in=data_in)
    await db.refresh(user)
    assert verify_password(new_password, user.hashed_password)
