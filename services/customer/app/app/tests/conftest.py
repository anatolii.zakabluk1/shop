from typing import Dict, Generator

import pytest
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.deps import get_db
from app.core.config import settings
from app.db.base import Base
from app.main import app
from app.tests.utils import event_loop as el
from app.tests.utils.init_db import init_db
from app.tests.utils.overrides import override_get_db
from app.tests.utils.test_db import TestingAsyncSessionLocal, engine
from app.tests.utils.user import (get_user_token_headers_from_email,
                                  get_superuser_token_headers)


app.dependency_overrides[get_db] = override_get_db


@pytest.fixture(scope="session")
async def db() -> Generator:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)

    async with TestingAsyncSessionLocal() as db:
        await init_db(db)
        yield db


@pytest.fixture(scope="module")
async def client(db) -> Generator:
    async with AsyncClient(app=app, base_url="http://test") as ac:
        yield ac


@pytest.fixture(scope="session")
def event_loop() -> Generator:
    loop = el.get_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="module")
async def superuser_token_headers(
    client: AsyncClient, db: AsyncSession
) -> Dict[str, str]:
    return await get_superuser_token_headers(client=client, db=db)


@pytest.fixture(scope="module")
async def normal_user_token_headers(
    client: AsyncClient, db: AsyncSession
) -> Dict[str, str]:
    return await get_user_token_headers_from_email(
        client=client, email=settings.EMAIL_TEST_USER, db=db
    )
