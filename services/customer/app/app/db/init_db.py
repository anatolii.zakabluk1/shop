from sqlalchemy.ext.asyncio import AsyncSession

from app import crud, schemas
from app.core.config import settings
from app.db import base  # noqa: F401
from app.db.utils import load_json_fixture

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28

TEST_PASSWORD = 'test'


class TestUserCreate(schemas.UserCreate):
    id: int
    new_password1: str = TEST_PASSWORD
    new_password2: str = TEST_PASSWORD


async def init_db(db: AsyncSession) -> None:
    if not await crud.user.count(db):
        # Create superuser
        user_in = schemas.UserCreate(
            email=settings.FIRST_SUPERUSER,
            new_password1=settings.FIRST_SUPERUSER_PASSWORD,
            new_password2=settings.FIRST_SUPERUSER_PASSWORD,
            is_superuser=True,
        )
        user = await crud.user.create(db, obj_in=user_in)  # noqa: F841

        # Load test users data to db
        if users := load_json_fixture('users'):
            for user in users:
                user_in = TestUserCreate(**user)
                user = await crud.user.create(db, obj_in=user_in)
