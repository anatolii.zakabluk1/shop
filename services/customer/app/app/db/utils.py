import json
import os
from typing import Dict, Any


def load_json_fixture(name: str) -> Dict[str, Any] | None:
    file_name = name if name.split('.')[-1] == 'json' else f'{name}.json'
    file_path = os.path.join('.', 'app', 'db', 'fixtures', file_name)
    with open(file_path) as f:
        return json.load(f)
