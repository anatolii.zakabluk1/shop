from sqlalchemy import Boolean, Column, Integer, String

from app.db.base_class import Base
from app.models.mixins import TimestampMixin


class User(TimestampMixin, Base):
    id = Column(Integer, primary_key=True, index=True)
    full_name = Column(String, index=True, nullable=False, default='')
    email = Column(String, unique=True, index=True, nullable=False)
    phone = Column(String(30), nullable=False, default='')
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
