from pydantic import BaseModel, EmailStr, validator

from app.schemas.validators import passwords_match, str_must_contain_space


# Shared properties
class UserBase(BaseModel):
    email: EmailStr | None = None
    is_active: bool | None = True
    is_superuser: bool = False
    full_name: str | None = None
    phone: str | None = None


class PasswordBase(BaseModel):
    new_password1: str
    new_password2: str

    _passwords_match = validator(
        'new_password2', allow_reuse=True)(passwords_match)


class UserRegister(PasswordBase):
    email: EmailStr


# Properties to receive via API on creation
class UserCreate(UserBase, PasswordBase):
    email: EmailStr

    _name_must_contain_space = validator(
        'full_name', allow_reuse=True)(str_must_contain_space)


# Properties to receive via API on update
class UserUpdate(UserBase):
    _name_must_contain_space = validator(
        'full_name', allow_reuse=True)(str_must_contain_space)


class UserMeUpdate(BaseModel):
    email: EmailStr | None = None
    full_name: str | None = None
    phone: str | None = None

    _name_must_contain_space = validator(
        'full_name', allow_reuse=True)(str_must_contain_space)


# Properties to receive via API on password update
class UserPasswordUpdate(PasswordBase):
    old_password: str


# Properties to receive via API on retrieve
class User(UserBase):
    id: int | None = None

    class Config:
        orm_mode = True
