def str_must_contain_space(str: str | None) -> str:
    if str:
        if ' ' not in str:
            raise ValueError('Must contain a space')
        return str.title()
    else:
        return str


def passwords_match(v, values, **kwargs):
    if v != values['new_password1']:
        raise ValueError('Passwords do not match')
    return v
