FROM python:3.10-slim as requirements-stage

WORKDIR /tmp

RUN pip install poetry
COPY ./pyproject.toml ./poetry.lock* /tmp/
# Allow installing dev dependencies to run tests
ARG INSTALL_DEV=false
RUN bash -c "if [ $INSTALL_DEV == 'true' ] ; then \
                poetry export -f requirements.txt --output requirements.txt --without-hashes --dev; \
             else \
                poetry export -f requirements.txt --output requirements.txt --without-hashes; \
             fi"


FROM python:3.10-slim

ENV PROJECT_ROOT=/usr/src/app

WORKDIR $PROJECT_ROOT

ENV PYTHONPATH=/usr/src/app
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY --from=requirements-stage /tmp/requirements.txt $PROJECT_ROOT/requirements.txt
RUN pip install --no-cache-dir --upgrade -r $PROJECT_ROOT/requirements.txt

COPY ./app $PROJECT_ROOT

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]