from fastapi import status
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from app import schemas, crud
from app.db import data
from app.core.config import settings
from app.tests.utils.user import get_random_user


async def test_read_cart_mine(
    client: AsyncClient, current_user: schemas.User, db: AsyncSession
) -> None:
    r = await client.get(f"{settings.API_V1_STR}/carts/mine")
    assert r.status_code == status.HTTP_200_OK
    api_user_cart = r.json()
    assert api_user_cart["total"] == 0.0
    assert not api_user_cart["items"]

    product = data.products[1]
    db_user_cart = await crud.cart.get_or_create_user_cart(db, user_id=current_user.id)
    db_user_cart = await crud.cart.add_item(
        db, cart=db_user_cart, product=schemas.Product(**product)
    )
    r = await client.get(f"{settings.API_V1_STR}/carts/mine")
    assert r.status_code == status.HTTP_200_OK
    api_user_cart = r.json()
    assert float(db_user_cart.total) == api_user_cart["total"]
    assert len(db_user_cart.items) == len(api_user_cart["items"])
    assert db_user_cart.items[-1].id == api_user_cart["items"][-1]["id"]
    assert db_user_cart.items[-1].product_id == api_user_cart["items"][-1]["product_id"]
    assert db_user_cart.items[-1].name == api_user_cart["items"][-1]["name"]
    assert db_user_cart.items[-1].desc == api_user_cart["items"][-1]["desc"]
    assert float(db_user_cart.items[-1].price) == api_user_cart["items"][-1]["price"]


async def test_read_cart_by_id(
    client: AsyncClient, current_user: schemas.User, db: AsyncSession
):
    random_user = get_random_user()
    db_random_user_cart = await (
        crud.cart.get_or_create_user_cart(db, user_id=random_user.id)
    )
    r = await client.get(f"{settings.API_V1_STR}/carts/{db_random_user_cart.id}")
    assert r.status_code == status.HTTP_403_FORBIDDEN
    assert r.json() == {"detail": "Not enough permissions"}

    r = await client.get(f"{settings.API_V1_STR}/carts/{db_random_user_cart.id + 1}")
    assert r.status_code == status.HTTP_404_NOT_FOUND
    assert r.json() == {"detail": "Cart not found"}

    db_current_user_cart = await (
        crud.cart.get_or_create_user_cart(db, user_id=current_user.id)
    )
    r = await client.get(f"{settings.API_V1_STR}/carts/{db_current_user_cart.id}")
    assert r.status_code == status.HTTP_200_OK
    assert r.json()["id"] == db_current_user_cart.id


async def test_add_item_to_cart_mine(
    client: AsyncClient, current_user: schemas.User, db: AsyncSession
) -> None:
    product = data.products[2]
    db_user_cart_before = await crud.cart\
        .get_or_create_user_cart(db, user_id=current_user.id)
    r = await client.post(f"{settings.API_V1_STR}/carts/mine/items",
                          json={"product_id": product["id"]})
    assert r.status_code == status.HTTP_200_OK
    api_user_cart = r.json()
    assert float(db_user_cart_before.total) + product["price"] == api_user_cart["total"]
    assert len(db_user_cart_before.items) + 1 == len(api_user_cart["items"])
    assert api_user_cart["items"][-1]["product_id"] == product["id"]
    assert api_user_cart["items"][-1]["name"] == product["name"]
    assert api_user_cart["items"][-1]["desc"] == product["desc"]
    assert api_user_cart["items"][-1]["price"] == product["price"]

    r = await client.post(f"{settings.API_V1_STR}/carts/mine/items",
                          json={"product_id": product["id"]})
    assert r.status_code == status.HTTP_409_CONFLICT
    assert r.json() == {"detail": "Cart item already exist"}

    r = await client.post(f"{settings.API_V1_STR}/carts/mine/items",
                          json={"product_id": len(data.products) + 1})
    assert r.status_code == status.HTTP_404_NOT_FOUND
    assert r.json() == {"detail": "Product not found"}


async def test_clear_cart_mine(
    client: AsyncClient, current_user: schemas.User, db: AsyncSession
) -> None:
    db_user_cart = await crud.cart.get_or_create_user_cart(db, user_id=current_user.id)
    db_user_cart = await crud.cart.add_item(
        db, cart=db_user_cart, product=schemas.Product(**data.products[3])
    )
    assert db_user_cart.total > 0
    assert len(db_user_cart.items) > 0

    r = await client.delete(f"{settings.API_V1_STR}/carts/mine/clear")
    r.status_code = status.HTTP_200_OK
    api_user_cart = r.json()
    await db.refresh(db_user_cart)
    assert db_user_cart.total == 0 and len(db_user_cart.items) == 0
    assert api_user_cart["total"] == 0 and len(api_user_cart["items"]) == 0


async def test_delete_item_from_cart_mine(
    client: AsyncClient, current_user: schemas.User, db: AsyncSession
) -> None:
    db_user_cart = await crud.cart.get_or_create_user_cart(
        db, user_id=current_user.id
    )
    product = data.products[4]
    db_user_cart = await crud.cart.add_item(
        db, cart=db_user_cart, product=schemas.Product(**product)
    )
    r = await client.get(f"{settings.API_V1_STR}/carts/mine")
    assert db_user_cart.items[-1].product_id == product["id"]
    assert r.json()["items"][-1]["product_id"] == product["id"]

    added_item = db_user_cart.items[-1]
    r = await client.delete(f"{settings.API_V1_STR}/carts/mine/items/{added_item.id}")
    api_user_cart = r.json()
    assert added_item.id not in {item["id"] for item in api_user_cart["items"]}
    assert api_user_cart["total"] == float(db_user_cart.total) - product["price"]
    assert len(api_user_cart["items"]) == len(db_user_cart.items) - 1

    await db.refresh(db_user_cart)

    assert added_item.id not in {item.id for item in db_user_cart.items}
    assert api_user_cart["total"] == float(db_user_cart.total)
    assert len(api_user_cart["items"]) == len(db_user_cart.items)

    r = await client.delete(f"{settings.API_V1_STR}/carts/mine/items/{added_item.id}")
    assert r.status_code == status.HTTP_404_NOT_FOUND
    assert r.json() == {"detail": "Item not found"}


async def test_update_cart_item_qty_mine(
    client: AsyncClient, current_user: schemas.User, db: AsyncSession
) -> None:
    db_user_cart = await crud.cart.get_or_create_user_cart(
        db, user_id=current_user.id
    )
    product = data.products[5]
    db_user_cart = await crud.cart.add_item(
        db, cart=db_user_cart, product=schemas.Product(**product)
    )
    added_item = db_user_cart.items[-1]
    assert db_user_cart.items[-1].product_id == product["id"]

    r = await (
        client.put(
            f"{settings.API_V1_STR}/carts/mine/items/{added_item.id}/qty",
            json={"quantity": 10}
        )
    )
    assert r.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    response_data = r.json()
    msg = "Cart can not contain more than 5 identical products"
    assert response_data["detail"][0]["msg"] == msg
    assert response_data["detail"][0]["type"] == "value_error"

    r = await (
        client.put(
            f"{settings.API_V1_STR}/carts/mine/items/{added_item.id}/qty",
            json={"quantity": -1}
        )
    )
    assert r.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    response_data = r.json()
    assert response_data["detail"][0]["msg"] == "Must be positive"
    assert response_data["detail"][0]["type"] == "value_error"

    new_quantity = 3
    r = await (
        client.put(
            f"{settings.API_V1_STR}/carts/mine/items/{added_item.id}/qty",
            json={"quantity": 3}
        )
    )
    assert r.status_code == status.HTTP_200_OK
    api_user_cart = r.json()
    new_product_price = float(added_item.price) * (new_quantity - added_item.quantity)
    assert api_user_cart["total"] == float(db_user_cart.total) + new_product_price

    r = await (
        client.put(
            f"{settings.API_V1_STR}/carts/mine/items/{added_item.id + 1}/qty",
            json={"quantity": 2}
        )
    )
    assert r.status_code == status.HTTP_404_NOT_FOUND
    assert r.json() == {"detail": "Item not found"}
