from typing import Generator

import pytest
from httpx import AsyncClient

from app.api.deps import get_db, get_current_user
from app.db.base import Base
from app.main import app
from app.schemas.user import User
from app.tests.utils import event_loop as el
from app.tests.utils.overrides import override_get_db, override_get_current_user
from app.tests.utils.test_db import TestingAsyncSessionLocal, engine


app.dependency_overrides[get_db] = override_get_db
app.dependency_overrides[get_current_user] = override_get_current_user


@pytest.fixture(scope="session")
async def db() -> Generator:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)

    async with TestingAsyncSessionLocal() as db:
        yield db


@pytest.fixture(scope="module")
async def client(db) -> Generator:
    async with AsyncClient(app=app, base_url="http://test") as ac:
        yield ac


@pytest.fixture(scope="session")
def event_loop() -> Generator:
    loop = el.get_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
async def current_user() -> User:
    return await override_get_current_user()
