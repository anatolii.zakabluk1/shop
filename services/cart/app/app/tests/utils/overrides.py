from typing import Generator

from app.tests.utils.test_db import TestingAsyncSessionLocal
from app.schemas.user import User


async def override_get_db() -> Generator:
    async with TestingAsyncSessionLocal() as session:
        yield session


async def override_get_current_user() -> User:
    return User(id=1, email="testuser@example.com")
