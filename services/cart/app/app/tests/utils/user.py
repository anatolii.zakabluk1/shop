from random import randint

from app.tests.utils.utils import random_email
from app.schemas.user import User


def get_random_user() -> User:
    return User(id=randint(2, 10000000), email=random_email())
