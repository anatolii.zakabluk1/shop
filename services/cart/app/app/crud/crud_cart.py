from fastapi.encoders import jsonable_encoder
from sqlalchemy import select, delete, func
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from app import schemas
from app.crud.exceptions import ItemAlreadyExist
from app.models.cart import Cart
from app.models.cart_item import CartItem


class CRUDCart:
    async def get(self, db: AsyncSession, id: int) -> Cart | None:
        result = await db.execute(select(Cart).filter(Cart.id == id))
        return result.scalars().first()

    async def get_by_user(self, db: AsyncSession, *, user_id: int) -> Cart | None:
        result = await db.execute(
            select(Cart).filter(Cart.user_id == user_id).order_by(Cart.id.desc())
        )
        return result.scalars().first()

    async def get_or_create_user_cart(self, db: AsyncSession, *, user_id: int) -> Cart:
        cart = await self.get_by_user(db, user_id=user_id)
        if not cart:
            cart = Cart(user_id=user_id)
            db.add(cart)
            await db.commit()
            await db.refresh(cart)
        return cart

    async def get_item(
        self, db: AsyncSession, *, cart_id: int, item_id: int
    ) -> CartItem | None:
        result = await db.execute(
            select(CartItem).filter(CartItem.cart_id == cart_id, CartItem.id == item_id)
        )
        return result.scalars().first()

    async def add_item(
        self, db: AsyncSession, *, cart: Cart, product: schemas.Product,
    ) -> Cart:
        product_data = jsonable_encoder(product)
        product_id = product_data.pop("id")
        cart_item = CartItem(
            cart_id=cart.id,
            product_id=product_id,
            **product_data
        )
        db.add(cart_item)
        cart.total += product.price
        try:
            await db.commit()
        except IntegrityError:
            await db.rollback()
            raise ItemAlreadyExist
        await db.refresh(cart)
        return cart

    async def update_item_qty(
        self, db: AsyncSession, *, cart: Cart, item: CartItem, qty: int
    ) -> Cart:
        cart.total += item.price * (qty - item.quantity)
        item.quantity = qty
        await db.commit()
        await db.refresh(cart)
        return cart

    async def remove_item(
        self, db: AsyncSession, *, cart: Cart, item: CartItem
    ) -> Cart:
        await db.delete(item)
        cart.total -= item.price * item.quantity
        await db.commit()
        await db.refresh(cart)
        return cart

    async def clear(self, db: AsyncSession, *, cart: Cart) -> Cart:
        await db.execute(
            delete(CartItem).filter(CartItem.cart_id == cart.id)
                .execution_options(synchronize_session=False)
        )
        cart.total = 0
        await db.commit()
        await db.refresh(cart)
        return cart

    async def count(self, db: AsyncSession) -> int:
        result = await db.execute(select(func.count(Cart.id)))
        return result.scalar_one()


cart = CRUDCart()
