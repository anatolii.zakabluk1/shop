from typing import TYPE_CHECKING

from sqlalchemy import (Column, Integer, SmallInteger, ForeignKey, Numeric, String,
                        Text, UniqueConstraint)
from sqlalchemy.orm import relationship

from app.db.base_class import Base
from app.models.mixins import TimestampMixin


if TYPE_CHECKING:
    from .cart import Cart  # noqa: F401
    from .product import Product  # noqa: F401


class CartItem(TimestampMixin, Base):
    id = Column(Integer, primary_key=True, index=True)
    cart_id = Column(Integer, ForeignKey("cart.id"), nullable=False)
    product_id = Column(Integer, nullable=False)
    price = Column(Numeric(8, 2), nullable=False)
    name = Column(String(100), nullable=False)
    desc = Column(Text, default="", nullable=False)
    quantity = Column(SmallInteger, default=1, nullable=False)
    cart = relationship("Cart", back_populates="items")

    __table_args__ = (
        UniqueConstraint('cart_id', 'product_id',
                         name="cartitem_card_id_product_id_key"),
    )
