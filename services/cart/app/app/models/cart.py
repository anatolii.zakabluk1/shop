from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, Numeric
from sqlalchemy.orm import relationship

from app.db.base_class import Base
from app.models.mixins import TimestampMixin


if TYPE_CHECKING:
    from .cart_item import CartItem  # noqa: F401


class Cart(TimestampMixin, Base):
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, unique=True, index=True, nullable=False)
    total = Column(Numeric(11, 2), default=0.0, nullable=False)
    items = relationship("CartItem", back_populates="cart", lazy='joined')
