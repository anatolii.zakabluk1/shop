from pydantic import BaseModel, EmailStr


class User(BaseModel):
    id: int
    email: EmailStr
    is_active: bool = True
    is_superuser: bool = False
    full_name: str = None
    phone: str = None
