from decimal import Decimal

from pydantic import BaseModel


class Product(BaseModel):
    id: int
    name: str
    desc: str
    price: Decimal

    class Config:
        orm_mode = True


class ProductAdd(BaseModel):
    product_id: int
