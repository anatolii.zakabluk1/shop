from decimal import Decimal
from pydantic import BaseModel, validator


class CartItem(BaseModel):
    id: int
    product_id: int
    name: str
    price: Decimal
    desc: str
    quantity: int

    class Config:
        orm_mode = True


class CartItemQtyUpdate(BaseModel):
    quantity: int

    @validator('quantity')
    def check_positive(cls, v: int):
        if v <= 0:
            raise ValueError('Must be positive')
        return v

    @validator('quantity')
    def max_value(cls, v: int):
        if v > 5:
            raise ValueError('Cart can not contain more than 5 identical products')
        return v
