from decimal import Decimal
from typing import List

from pydantic import BaseModel

from app.schemas.cart_item import CartItem


class Cart(BaseModel):
    id: int | None = None
    total: Decimal
    items: List[CartItem] = []

    class Config:
        orm_mode = True
