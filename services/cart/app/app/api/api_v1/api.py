from fastapi import APIRouter

from app.api.api_v1.endpoints import cart

api_router = APIRouter()
api_router.include_router(cart.router, prefix="/carts", tags=["carts"])
