from typing import Any

from fastapi import APIRouter, Depends, status, HTTPException, Path
from sqlalchemy.ext.asyncio import AsyncSession

from app import schemas, crud
from app.api import deps
from app.db import data
from app.crud.exceptions import ItemAlreadyExist


router = APIRouter()

__valid_id = Path(..., ge=1)


@router.get("/mine", response_model=schemas.Cart)
async def read_cart_mine(
    db: AsyncSession = Depends(deps.get_db),
    current_user: schemas.User = Depends(deps.get_current_active_user)
) -> Any:
    cart = await crud.cart.get_by_user(db, user_id=current_user.id)
    if not cart:
        return schemas.Cart(total=0.0)  # Empty cart
    return cart


@router.get("/{cart_id}", response_model=schemas.Cart)
async def read_cart_by_id(
    *,
    db: AsyncSession = Depends(deps.get_db),
    cart_id: int = __valid_id,
    current_user: schemas.User = Depends(deps.get_current_active_user)
) -> Any:
    cart = await crud.cart.get(db, id=cart_id)
    if not cart:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Cart not found",
        )
    if cart.user_id == current_user.id:
        return cart
    if not current_user.is_superuser:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Not enough permissions"
        )
    return cart


@router.post("/mine/items", response_model=schemas.Cart)
async def add_item_to_cart_mine(
    *,
    db: AsyncSession = Depends(deps.get_db),
    product_in: schemas.ProductAdd,
    current_user: schemas.User = Depends(deps.get_current_active_user),
) -> Any:
    product = data.products.get(product_in.product_id)
    if not product:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Product not found",
        )
    product = schemas.Product(**product)
    cart = await crud.cart.get_or_create_user_cart(db, user_id=current_user.id)
    try:
        cart = await crud.cart.add_item(db, cart=cart, product=product)
    except ItemAlreadyExist:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="Cart item already exist",
        )
    return cart


@router.delete("/mine/clear", response_model=schemas.Cart)
async def clear_cart_mine(
    *,
    db: AsyncSession = Depends(deps.get_db),
    current_user: schemas.User = Depends(deps.get_current_active_user),
) -> Any:
    cart = await crud.cart.get_by_user(db, user_id=current_user.id)
    if not cart:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not have active cart",
        )

    cart = await crud.cart.clear(db, cart=cart)
    return cart


@router.delete("/mine/items/{item_id}", response_model=schemas.Cart)
async def delete_item_from_cart_mine(
    *,
    db: AsyncSession = Depends(deps.get_db),
    item_id: int = __valid_id,
    current_user: schemas.User = Depends(deps.get_current_active_user),
) -> Any:
    cart = await crud.cart.get_by_user(db, user_id=current_user.id)
    if not cart:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not have active cart",
        )
    item = await crud.cart.get_item(db, cart_id=cart.id, item_id=item_id)
    if not item:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Item not found"
        )

    cart = await crud.cart.remove_item(db, cart=cart, item=item)
    return cart


@router.put("/mine/items/{item_id}/qty", response_model=schemas.Cart)
async def update_cart_item_qty_mine(
    *,
    db: AsyncSession = Depends(deps.get_db),
    item_id: int = __valid_id,
    item_qty_in: schemas.CartItemQtyUpdate,
    current_user: schemas.User = Depends(deps.get_current_active_user),
) -> Any:
    cart = await crud.cart.get_by_user(db, user_id=current_user.id)
    if not cart:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User not have active cart",
        )
    item = await crud.cart.get_item(db, cart_id=cart.id, item_id=item_id)
    if not item:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Item not found"
        )

    cart = await crud.cart.update_item_qty(
        db, cart=cart, item=item, qty=item_qty_in.quantity
    )
    return cart
