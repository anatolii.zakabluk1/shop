from typing import Generator

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from httpx import AsyncClient, HTTPStatusError
from pydantic import ValidationError

from app.core.config import settings
from app.db.session import AsyncSessionLocal
from app.schemas.user import User


reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl=f"{settings.CUSTOMER_SERVICE_LOGIN_URL}"
)


async def get_db() -> Generator:
    async with AsyncSessionLocal() as session:
        yield session


async def client() -> AsyncClient:
    async with AsyncClient() as c:
        yield c


async def get_current_user(
    client: AsyncClient = Depends(client),
    token: str = Depends(reusable_oauth2)
) -> User:
    try:
        response = await client.get(
            url=f"{settings.CUSTOMER_SERVICE_API_URL}/users/me",
            headers={
                "Authorization": f"Bearer {token}",
            },
        )

        response.raise_for_status()
        return User(**response.json())
    except HTTPStatusError as error:
        r = error.response.json()
        raise HTTPException(
            status_code=error.response.status_code,
            detail=r.get("detail")
        )
    except ValidationError:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials",
        )


async def get_current_active_user(
    current_user: User = Depends(get_current_user),
) -> User:
    if not current_user.is_active:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Inactive user"
        )
    return current_user
