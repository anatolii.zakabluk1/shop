products = {
    1: {
        "id": 1,
        "name": "iPhone 13",
        "desc": "iPhone 13 is the last generation smartphone.",
        "price": 1299.99
    },
    2: {
        "id": 2,
        "name": "Xiaomi Poco F2 Pro",
        "desc": "Xiaomi is the last generation smartphone.",
        "price": 799.89
    },
    3: {
        "id": 3,
        "name": "LG K45",
        "desc": "LG is the last generation smartphone.",
        "price": 859.89
    },
    4: {
        "id": 4,
        "name": "Xiaomi Redmi Note 9",
        "desc": "LG is the last generation smartphone.",
        "price": 399.99
    },
    5: {
        "id": 5,
        "name": "Asus ROG Zephyrus G GA502DU",
        "desc": "Powerful laptop.",
        "price": 28999.00
    },
    6: {
        "id": 6,
        "name": "Lenovo legion 5",
        "desc": "Modern solution.",
        "price": 28999.00
    },
    7: {
        "id": 7,
        "name": "Acer nitro 5",
        "desc": "Acer Nitro 5 (2020) is a laptop with a 15.00-inch display.",
        "price": 28999.00
    }
}
