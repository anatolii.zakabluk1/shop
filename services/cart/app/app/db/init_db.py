import random

from sqlalchemy.ext.asyncio import AsyncSession

from app import crud, schemas
from app.core.config import settings
from app.db import base, data  # noqa: F401

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28


async def init_db(db: AsyncSession) -> None:
    if not await crud.cart.count(db):
        # Load test carts data to db which are based on test users data
        # from customer service.
        for user_id in range(2, 7):
            cart = await crud.cart.get_or_create_user_cart(db, user_id=user_id)
            for product_id in random.sample(range(1, len(data.products)), 3):
                product = schemas.Product(**data.products[product_id])
                cart = await crud.cart.add_item(db, cart=cart, product=product)

